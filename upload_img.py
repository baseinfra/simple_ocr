from flask import Flask, render_template, request, redirect, flash
from werkzeug.utils import secure_filename
import os
import cv2
import numpy as np
import pytesseract

app = Flask(__name__)

UPLOAD_FOLDER = '/tmp'
IMG_FILENAME_PREFIX = 'ocr-img-'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route("/")
def home():
    return render_template("index.xhtml")

@app.route("/upload-image", methods=['POST'])
def uploadImage():
    # check if the post request has the file part
    if 'file' not in request.files:
        flash('No file part')
        return redirect("/")
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        flash('No selected file')
        return redirect("/")

    filename = IMG_FILENAME_PREFIX + secure_filename(file.filename)
    abs_filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file.save(abs_filepath)

    return get_text_from_image(filename=abs_filepath)
    
    #return redirect("/")

def get_text_from_image(filename):
    # Read image from which text needs to be extracted
    img = cv2.imread(filename)

    # Preprocessing the image starts

    # Convert the image to gray scale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Performing OTSU threshold
    ret, thresh1 = cv2.threshold(gray, 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)

    # Specify structure shape and kernel size.
    # Kernel size increases or decreases the area
    # of the rectangle to be detected.
    # A smaller value like (10, 10) will detect
    # each word instead of a sentence.
    rect_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (64, 64))

    # Appplying dilation on the threshold image
    dilation = cv2.dilate(thresh1, rect_kernel, iterations=1)

    # Finding contours
    _, contours, hierarchy = cv2.findContours(dilation, cv2.RETR_EXTERNAL,
                                              cv2.CHAIN_APPROX_NONE)

    # Creating a copy of image
    im2 = img.copy()

    doc_text = ""

    # Looping through the identified contours
    # Then rectangular part is cropped and passed on
    # to pytesseract for extracting text from it
    # Extracted text is then written into the text file
    contours.reverse()
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)

        # Drawing a rectangle on copied image
        rect = cv2.rectangle(im2, (x, y), (x + w, y + h), (0, 255, 0), 2)

        # Cropping the text block for giving input to OCR
        cropped = im2[y:y + h, x:x + w]

        # Apply OCR on the cropped image
        text = pytesseract.image_to_string(cropped)
        print(text)

        doc_text += text + "<br/><br/>"
    
    return doc_text

if __name__ == "__main__":
    app.run(debug=True)
